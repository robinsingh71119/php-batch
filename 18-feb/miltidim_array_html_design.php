<?php

$data = array(
    array('name'=>'jarvis','roll_no'=>rand(10,99),'course'=>'PHP'),
    array('name'=>'james','roll_no'=>rand(10,99),'course'=>'HTML'),
    array('name'=>'sophia','roll_no'=>rand(10,99),'course'=>'PYTHON'),
    array('name'=>'harry','roll_no'=>rand(10,99),'course'=>'CSS'),
);

print_r($data);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
</head>
<body>

    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>S no</th>
                <th>Name</th>
                <th>Roll no</th>
                <th>Course</th>
            </tr>
        </thead>
        <tbody>
            <?php
            for($i=0 ; $i<sizeof($data) ; $i++){
                echo "<tr>";
                echo "<td>".($i+1)."</td>";
                echo "<td>".$data[$i]['name']."</td>";
                echo "<td>".$data[$i]['roll_no']."</td>";
                echo "<td>".$data[$i]['course']."</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
    
</body>
</html>