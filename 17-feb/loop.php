<?php 

// echo 'even from 1 to 20';

// for($i=0 ; $i<10 ; $i++){
//     if($i%2 ==1){
//         echo '<h1>'.$i.'</h1>';
//     }
// }

// while
// $x = 1;
// while($x<10){
//     echo '<h1>'.$x.'</h1>';
//     $x++;
// }


// Find the sum of 1 to 100 using While Loop
// $i = 0;
// $sum=0;
// while($i<=100){
//     if($i%2==0){
//         $sum += $i;
//     }
//     $i++;
//     }
// echo "<h2>Sum of 1 to 100 number is = <ins>".$sum."</ins></h2>";
// do while
$i=10;
do{
    echo '<h1>'.$i.'</h1>';
    $i--;
}
while($i>=1);

// foreach()
?>