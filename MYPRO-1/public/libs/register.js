// alert(baseUrl)
window.onload = function(){
    getData();
}
register =()=>{
    let uName = $('#uName').val()
    let email = $('#email').val()
    let phone = $('#phone').val()
    let password = $('#password').val()
    let token = $('input[name="_token"]').val()
    let profile_img = document.getElementById('profile_img').files[0]
    // console.log(profile_img)
    $('#msg').html('')
    $('#msg').removeClass('alert-danger').removeClass('alert-success')
    let url = 'newSignUp';
    let formData = new FormData();
    formData.append('_token',token);
    formData.append('username',uName);
    formData.append('email',email);
    formData.append('phone_no',phone);
    formData.append('password',password);
    formData.append('profile_img',profile_img);
    let xhr = new XMLHttpRequest();
    xhr.open('POST',url)
    xhr.send(formData)
    xhr.onload = function(){
        // console.log(typeof(xhr.responseText));
        let obj = JSON.parse(xhr.responseText)
        console.log(obj); 
        let st = obj.status;
        let me = obj.message;
        if(st == false){
            $('#msg').addClass('alert-danger')
            $('#msg').html(me)
            return false;
        }
        $('#msg').removeClass('alert-danger').addClass('alert-success')
        $('#msg').html(me)
        getData()
    }
}

getData =()=>{
    let url = 'newGetData';
    let xhr = new  XMLHttpRequest();
    xhr.open('get',url);
    xhr.send('');
    xhr.onload = function(){
        // console.log(typeof(xhr.responseText));
        let obj = JSON.parse(xhr.responseText)
        console.log(obj); 
        let st = obj.status;
        let me = obj.message;
        if(st == false){
            
            return false;
        }
        tag_name = ''
        let data =  obj.data
        for(i=0 ; i<data.length ; i++){
            tag_name += '<tr>'
                tag_name += '<td>'+(i+1)+'</td>'
                tag_name += '<td>'+data[i].username+'</td>'
                tag_name += '<td>'+data[i].email+'</td>'
                tag_name += '<td>'+data[i].phone_no+'</td>'
                tag_name += '<td>'+data[i].password+'</td>'
            tag_name += '</tr>'
        }
        $('#tableData').html(tag_name)
        
        
    }
}