loginCheck = ()=>{
    let email = $('#email').val()
    let password = $('#password').val()
    let remember_me = 0
    let token = $('input[name="_token"]').val()
    if(email == '' || password == ''){
        $('#loginMsg').html('All fields are required')
        $('#loginMsg').css('color','red')
        return false;
    }
    if ($('#remember_me').is(':checked')) {
        remember_me = 1
    }
    let url = "loginCheck";
    let formData = new FormData()
    formData.append('_token',token);
    formData.append('email',email)
    formData.append('password',password)
    formData.append('remember_me',remember_me)
    let xhr = new XMLHttpRequest
    xhr.open('POST',url)
    xhr.send(formData)
    xhr.onload = function(){
        let Obj = JSON.parse(xhr.responseText);
        let st = Obj.status;
        let me = Obj.message;
        if(!st){
            $('#loginMsg').html(me)
            $('#loginMsg').css('color','red')
            return false;
        }
        $('#loginMsg').html(me)
        $('#loginMsg').css('color','green')
        setTimeout(() => {
            window.location.href = 'dashboard'
        }, 2000);        
    }
}