<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use Cookie;

class LoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Cookie::has('useremail')){
            session()->flash('error','Need to login first to enter dasbhoard');
            return redirect('newLogin');    
        }
        return $next($request);
    }
}
