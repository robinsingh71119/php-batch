<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\RegisterModel;
use App\Models\VerifyEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Session;
use Cookie;
use File;

class NewSectionView extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('new_section.view_details');
    }

    public function signup()
    {
        return view('new_section.register');
    }

    public function Register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email'=>'required|unique:register_models',
            'password'=>'required',
        ]);
        // if($validate->fails()){
        //     return response()->json([
        //         'Status'=>false,
        //         'Message'=>$validate->errors()->first(),
        //     ],422);
        // }
        if($validator->fails()){
            return $this->send_error_message($validator->errors()->first());
        }   
        $pass = $request->password;
        $data = $request->all();
        unset($data['_token']);
        unset($data['password']);
        $data['password'] = Hash::make($pass);
        // file upload
        if($request->hasFile('profile_img')){
            $image = $request->profile_img;
            $extension = $image->getClientOriginalExtension();
            $newName = rand().time().'.'.$extension;
            $destination = public_path('profile_images'); // folder path and name
            $image->move($destination,$newName); // upload
            unset($data['profile_img']); // delete the request key
            $data['profile_image'] = $newName; // insert image name into request

        }
        $insertQuery = RegisterModel::create($data);
        // verification section
        $ve = new VerifyEmail();
        $token = rand(5,10).time();
        $ve->email = $request->email;
        $ve->token = $token;
        $ve->save();
        // die();
        // Mail
        $mail_array = array('email'=>$request->email,'token'=>$token);
        Mail::send('email.register_mail',$mail_array,function($m) use ($mail_array){
            $m->to($mail_array['email'])->subject('Registration done');
            $m->from('abc@gmail.com');
        });
        return $this->send_success_message('Data inserted successfully');        
    }

    public function newGetData()
    {
        $data = RegisterModel::all();
        return $this->send_success_message('All record',$data);
    }

    public function login_(Request $request){
        $checkEmail = RegisterModel::where('email',$request->email)->first();
        if($checkEmail){
            // check verification status
            if($checkEmail->status != 1){
                return $this->send_error_message('Email is not verified'); 
            }
            // password  check
            $checkPassword = Hash::check($request->password,$checkEmail->password);
            if($checkPassword){
                if($request->remember_me == 1){
                    // Cookie
                    Cookie::queue(Cookie::make('useremail',$checkEmail->email,20));
                    Cookie::queue(Cookie::make('userid',$checkEmail->id,20));
                    Cookie::queue(Cookie::make('usertype',$checkEmail->id,20));
                }else{
                    Session::put('useremail',$checkEmail->email);
                    Session::put('userid',$checkEmail->id);
                    Session::put('usertype',$checkEmail->id);
                }
                return $this->send_success_message('Login successfull');
            }
            return $this->send_error_message('Incorrect Password');
        }
        return $this->send_error_message('Email is not in our record'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dashboardView()
    {
        // if(Session::has('useremail')){
            $getLoginUserData = RegisterModel::where('email',Cookie::get('useremail'))->first();
            // print_r($getLoginUserData);
            // die();
            return view('new_section.dashboard')->with('data',$getLoginUserData);    
        // }
        // session()->flash('error','Need to login first to enter dasbhoard');
        // return redirect('newLogin');
    }

    public function profile_image_update(Request $request){
        // echo "<pre>";
        $getLoginUserData = RegisterModel::where('email',Cookie::get('useremail'))->first();
        // file exists code
        if($request->hasFile('pro_img')){
            $exists_image = public_path("/profile_images/".$getLoginUserData->profile_image);
            if(File::exists($exists_image)){
                File::delete($exists_image);
            }
            $new_image = $request->file('pro_img');
            $new_name = rand().time().'.'.$new_image->getClientOriginalExtension();
            $new_image->move(public_path('/profile_images/'),$new_name);
            $updateQuery = RegisterModel::where('email',Cookie::get('useremail'))->update(['profile_image'=>$new_name]);
        }
        return redirect('dashboard');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verify_email($token)
    {
        $getVerificationModalData = VerifyEmail::where('token',$token)->first();
        if($getVerificationModalData){
            $updateRegisterStatus = RegisterModel::where('email',$getVerificationModalData->email)->update(['status'=>1]);
            if($updateRegisterStatus){
                VerifyEmail::where('token',$token)->delete();
                session()->flash('success','Verification done successfully');
                return redirect('newLogin');
            }
            session()->flash('error','Verification not done successfully');
            return redirect('newLogin');
        }
        echo 'df';
    }

    public function viewProfile(){

        return view('new_section.dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        // Sessoin
        Session::flush();
        // Cookie
        Cookie::queue(Cookie::forget('usereamil'));
        Cookie::queue(Cookie::forget('userid'));
        Cookie::queue(Cookie::forget('usertype'));
        return redirect('newLogin');

    }
    public function send_success_message($message,$data=null){
        $resp = array();
        $resp['status']=true;
        $resp['message']=$message;
        if($data != null){
            $resp['data']=$data;
        }
        return $resp;
    }

    public function send_error_message($message){
        $resp = array();
        $resp['status']=false;
        $resp['message']=$message;
        return $resp;
    }
}
