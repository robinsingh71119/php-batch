<?php

namespace App\Http\Controllers;

use App\Models\RegisterModel;
use Illuminate\Http\Request;

class AllBackEnd extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // for register
    public function signupForm(Request $request)
    {
        // echo "<pre>";
        // print_r($request->all());
        // echo "</pre>";
        // die();
        // echo $request->username;die();
        $register = new RegisterModel();
        $register->username = $request->uName;
        $register->email    = $request->email;
        $register->phone_no = $request->phone;
        $register->password = $request->password;
        $register->save();
        session()->flash('success','Data inserted successfully');
        return redirect('signup');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editDetails($id)
    {
        // $getParticularData = RegisterModel::where('id',$id)->get();
        $getParticularData = RegisterModel::where('id',$id)->first();
        if($getParticularData){
            return view('edit_page')->with('editData',$getParticularData);
        }
        session()->flash('error','Details are not in DB');
        return redirect('signup');
        // echo "<pre>";
        // print_r($getParticularData);
        // echo "</pre>";
        // echo $id;
    }

    public function updateForm(Request $request)
    {
        
        $getParticularData = RegisterModel::where('id',$request->id_)->first();
        // echo "<pre>";
        // print_r($getParticularData);
        // echo "</pre>";
        // die();
        $getParticularData->username = $request->username;
        $getParticularData->email = $request->email;
        $getParticularData->phone_no = $request->number;
        $getParticularData->save();
        session()->flash('success','Data updated successfully');
        return redirect('signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteDetails($id)
    {
        $getParticularData = RegisterModel::where('id',$id)->first();
        if($getParticularData){
            $getParticularData->delete();
            session()->flash('success','Data deleted successfully');
            return redirect('signup');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
