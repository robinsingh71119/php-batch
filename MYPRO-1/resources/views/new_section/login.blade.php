@extends('new_section.layout')

@section('main_content')

    @if(Session::has('error'))
        <div class="alert alert-danger">{{Session::get('error')}}</div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <h2 class="text-center">Login</h2>
                <p id="loginMsg"></p>
                <div class="border p-4 mb-4">
                    @csrf
                    <input type="email" class="form-control mt-4" placeholder="Enter Email" id="email">
                    <input type="password" class="form-control mt-4" placeholder="Enter Password" id="password">
                    <input type="checkbox" id="remember_me" class="mt-4"> <span>Remember Me</span><br>
                    <input type="submit" onclick="loginCheck()" value="Login" class="btn btn-success my-4">
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('libs/login.js')}}"></script>

@endsection()