@extends('new_section.layout')

@section('main_content')

<h1>Dashboard</h1>
<img class="w-25" src="{{asset('/profile_images/'.$data->profile_image)}}" alt="profile_image">
<form action="{{url('profile_image_update')}}" method="post" enctype="multipart/form-data">
@csrf
    <input type="file" name='pro_img'><br>
    <input type="submit">
</form>

    @if(Session::has('useremail'))
        <h1 class="alert alert-success">Welcome  {{Session::get('useremail')}}  {{Session::get('userid')}} </h1>
    @elseif(Cookie::has('useremail'))
    <h1 class="alert alert-info">Welcome  {{Cookie::get('useremail')}}  {{Cookie::get('userid')}} </h1>
    @else
    <h2>Need to login first</h2>
    @endif

@endsection()