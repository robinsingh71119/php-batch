<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('font_awesome.css')}}">
    <script src="{{asset('libs/const.js')}}"></script>
    <style>
        .carousel-item  {
            width: 100%;
            height: 90vh;
        }
    </style>
</head>
<body>
    
    <div class="container-fluid">
        <div class="row bg-dark">
            <div class="col-md-12 border border-dark px-0">
                <nav class="navbar navbar-expand-md navbar-light bg-success">
                    <a href="" class="navbar-brand">BRAND LOGO</a>
                    <button data-target="#myNav" data-toggle="collapse" class="navbar-toggler">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="myNav">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item">
                                <a href="" class="nav-link">View Page</a>
                            </li>
                            @if((Session::has('useremail')) || (Cookie::has('useremail')))
                                <li class="nav-item">
                                    <a href="{{url ('logout')}}" class="nav-link">logout</a>
                                </li>
                            @else
                            <li class="nav-item">
                                <a href="{{url ('newRegister')}}" class="nav-link">Register</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url ('newLogin')}}" class="nav-link">Login</a>
                            </li>
                            @endif
                            
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="" class="nav-link"><i class="fas fa-user"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link"><i class="fab fa-instagram-square"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        @yield('main_content')
        <div class="row">
            <div class="col-md-12 bg-dark text-light"> 
                <h2>Footer</h2>
            </div>
        </div>
    </div>

    

</body>
</html>