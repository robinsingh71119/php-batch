@extends('new_section.layout')

@section('main_content')
        <div class="row">   
            <div class="col-md-8">
                <table class="table table-responsive">
                    <thead> 
                        <tr>
                            <th>S no</th>
                            <th>Name</th>
                            <th>Eamil</th>
                            <th>Phone</th>
                            <th>Password</th>
                        </tr>
                    </thead>
                    <tbody id="tableData">
                        
                    </tbody>
                </table>
            </div>
            <div class="col-md-4 mx-auto bg-dark text-light">
            @if(Session::has('error'))
                <div class="alert alert-success">{{Session::get('error')}}</div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-danger">{{Session::get('success')}}</div>
            @endif
                    <div id="msg" class="alert"></div>
                    <h1 class="text-center">Register</h1>
                    @csrf
                    <label for="">UserName</label>
                    <input type="text" placeholder="Enter First name" id="uName" class="form-control" required>
                    <label for="">Email</label>
                    <input type="email" placeholder="Enter Email" id="email" class="form-control" required>
                    <label for="">Phone</label>
                    <input type="number" placeholder="Enter Phone no" id="phone" class="form-control" required>
                    <label for="">Password</label>
                    <input type="password" placeholder="Enter Password" id="password" class="form-control" required>
                    <input type="file" id="profile_img" class="form-control my-4" required>
                    <input name="register" onclick="register()" type="submit" class="btn btn-success my-3">
            </div>
        </div>
    <script src="{{asset('libs/register.js')}}"></script>
@endsection