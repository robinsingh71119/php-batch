<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="{{url('updateForm')}}" method="post">
    @csrf
    <input type="hidden" name="id_" value="{{$editData['id']}}">
    <input type="text" name="username" value="{{$editData['username']}}">
    <input type="text" name="email" value="{{$editData['email']}}">
    <input type="text" name="number" value="{{$editData['phone_no']}}">
    <input type="submit">
</form>
    
</body>
</html>