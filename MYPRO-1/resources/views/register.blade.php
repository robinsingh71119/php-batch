@extends('layout.base')

@section('main_content')
        <div class="row">
            <div class="col-md-8">
                <table class="table table-bordered table-responsive table-sm">
                    <thead>
                        <tr>
                            <th>S no</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <td>{{$d['id']}}</td>
                            <td>{{$d['username']}}</td>
                            <td>{{$d['email']}}</td>
                            <td>{{$d['phone_no']}}</td>
                            <td>****************</td>
                            <td>
                                <a href="{{url('edit_page',$d['id'])}}"><button>Edit</button></a>
                                <a href="{{url('delete_page',$d['id'])}}"><button>Delete</button></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-4 mx-auto bg-dark text-light">
            @if(Session::has('error'))
                <div class="alert alert-success">{{Session::get('error')}}</div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-danger">{{Session::get('success')}}</div>
            @endif
                <h1 class="text-center">Register</h1>
                <form method="post" action="{{url('registerForm')}}">
                @csrf
                    <label for="">UserName</label>
                    <input type="text" placeholder="Enter First name" name="uName" class="form-control" required>
                    <label for="">Email</label>
                    <input type="email" placeholder="Enter Email" name="email" class="form-control" required>
                    <label for="">Phone</label>
                    <input type="number" placeholder="Enter Phone no" name="phone" class="form-control" required>
                    <label for="">Password</label>
                    <input type="password" placeholder="Enter Password" name="password" class="form-control" required>
                    <input name="register" type="submit" class="btn btn-success my-3">
                </form>
            </div>
        </div>
    </div>
@endsection