@extends('layout.base')

@section('main_content')

<div class="row">
    <div class="col-md-12">
        <div class="carousel slide" data-ride="carousel" id="myCarousel">
            <ul class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ul>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{asset('images/image.jpg')}}" alt="">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/Screenshot_2.png')}}" alt="">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/Screenshot_1.png')}}" alt="">
                </div>
                <a href="#myCarousel" class="carousel-control-prev" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a href="#myCarousel" class="carousel-control-next" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </div>
</div>

@endsection