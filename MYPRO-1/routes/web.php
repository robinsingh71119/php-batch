<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\View1Controller;
use App\Http\Controllers\AllBackEnd;
use App\Http\Controllers\NewSectionView;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::method('url',function(){
//     return 'sdfsdfs';
// });

// Route::method('url',['Controller:name,fn']);

// Route::view('home','first');
// Route::view('navbar','layout.base');

// main routes
// Route::get('',[ViewController::class,'showFirstFile']);
Route::get('',[View1Controller::class,'index']);
Route::get('signup',[View1Controller::class,'signup']);
Route::get('edit_page/{a}',[AllBackEnd::class,'editDetails']);
Route::get('delete_page/{a}',[AllBackEnd::class,'deleteDetails']);
// form routes
Route::post('registerForm',[AllBackEnd::class,'signupForm']);
Route::post('updateForm',[AllBackEnd::class,'updateForm']);


// New Section

Route::get('newViewPage',[NewSectionView::class,'index']);
Route::get('newRegister',[NewSectionView::class,'signup']);
Route::post('newSignUp',[NewSectionView::class,'Register']);
Route::get('newGetData',[NewSectionView::class,'newGetData']);
Route::view('newLogin','new_section.login');
Route::post('loginCheck',[NewSectionView::class,'login_']);

Route::get('logout',[NewSectionView::class,'logout']);
Route::get('verify_email/{token}',[NewSectionView::class,'verify_email']);


Route::group(['middleware'=>'myCustom'],function(){
    Route::get('dashboard',[NewSectionView::class,'dashboardView']);
    Route::post('profile_image_update',[NewSectionView::class,'profile_image_update']);
    Route::get('vp',[NewSectionView::class,'viewProfile']);
});
