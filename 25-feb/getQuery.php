<?php
session_start();
if($_SESSION){
    if($_SESSION['login']){
        echo $_SESSION['email'];
    }
}else{
    // echo '';
    exit('Plese Login , then only you can access this website <a href="../2-mar/login_.php">Click Here</a>');
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
</head>
<body>
    
<div class="container">
    <div class="row">
        <div class="col-md-10 mx-auto">
        <a href="../24-feb/insert_query.php"><button>Go back to Register form Page</button></a>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>S no</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    include('../24-feb/db_connect.php');
                    $getQuery = "SELECT * FROM register";
                    $result = $connect->query($getQuery);
                    if($result->num_rows > 0){
                        $count = 0;
                        while($row = $result->fetch_assoc()){
                            echo "<tr>";
                            echo "<th>".($count+1)."</th>";
                            echo "<th>".$row['username']."</th>";
                            echo "<th>".$row['email']."</th>";
                            echo "<th>*********</th>";
                            echo "<th>
                                    <a href='../1-mar/edit_query.php?id=".$row['id']."'><button class='btn btn-warning'>Edit</button></a>
                                    <a href='../1-mar/delete_query.php?id=".$row['id']."'><button class='btn btn-danger'>Delete</button></a>
                                  </th>";
                            echo "</tr>";
                            $count++;
                        }
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>