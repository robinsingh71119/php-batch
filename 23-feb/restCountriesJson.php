<?php

$url = file_get_contents('https://restcountries.eu/rest/v2/all');
$phpArr = json_decode($url,true);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
</head>
<body>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>S no</th>
                            <th>Country Name</th>
                            <th>Country Capital</th>
                            <th>Borders</th>
                            <th>Flag</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            for($i=0 ; $i<sizeof($phpArr) ; $i++){
                                $bor = $phpArr[$i]['borders'];
                                echo "<tr>";
                                echo "<td>".($i+1)."</td>";
                                echo "<td>".$phpArr[$i]['name']."</td>";
                                echo "<td>".$phpArr[$i]['capital']."</td>";
                                echo "<td><ul>";
                                foreach($bor as $d){
                                    echo '<li>'.$d."</li>";
                                }
                                echo "</ul></td>";
                                echo "<td><img src='".$phpArr[$i]['flag']."' alt='sdf' style='width:200px'></td>";
                                echo "</tr>";
                            }
                        
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</body>
</html>