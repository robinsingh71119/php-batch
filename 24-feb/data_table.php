<?php

$url = file_get_contents('https://restcountries.eu/rest/v2/all');
$phpArr = json_decode($url,true);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
</head>
<body>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-bordered" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th>S no</th>
                            <th>Country Name</th>
                            <th>Country Capital</th>
                            <th>Borders</th>
                            <th>Flag</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            for($i=0 ; $i<sizeof($phpArr) ; $i++){
                                $bor = $phpArr[$i]['borders'];
                                echo "<tr>";
                                echo "<td>".($i+1)."</td>";
                                echo "<td>".$phpArr[$i]['name']."</td>";
                                echo "<td>".$phpArr[$i]['capital']."</td>";
                                echo "<td><ul>";
                                foreach($bor as $d){
                                    echo '<li>'.$d."</li>";
                                }
                                echo "</ul></td>";
                                echo "<td><img src='".$phpArr[$i]['flag']."' alt='sdf' style='width:200px'></td>";
                                echo "</tr>";
                            }
                        
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>

</body>
</html>