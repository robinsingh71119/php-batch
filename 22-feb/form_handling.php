<?php

// if(isset($_GET['fName'])){
//     echo $_GET['fName'];
//     echo $_GET['lName'];
//     echo $_GET['email'];
//     echo $_GET['password'];
// }

if(isset($_POST['fName'])){
    echo $_POST['fName'];
    echo $_POST['lName'];
    echo $_POST['email'];
    echo $_POST['password'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto bg-dark text-light">
                <h1 class="text-center">Register</h1>
                <form method="post" action="showData.php">
                    <label for="">First Name</label>
                    <input type="text" placeholder="Enter First name" name="fName" class="form-control" required>
                    <label for="">Last Name</label>
                    <input type="text" placeholder="Enter Last name" name="lName" class="form-control">
                    <label for="">Email</label>
                    <input type="email" placeholder="Enter Email" name="email" class="form-control">
                    <label for="">Password</label>
                    <input type="password" placeholder="Enter Password" name="password" class="form-control">
                    <input name="register" type="submit" class="btn btn-success my-3">
                </form>
            </div>
        </div>
    </div>

    
</body>
</html>