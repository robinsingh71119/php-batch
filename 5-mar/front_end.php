<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
</head>
<body>

    <input type="text" id='username'>
    <input type="text" id='email'>
    <input type="text" id='password'>
    <button id="sign_up">Sign Up</button>
    <table>
        <thead>
            <tr>
                <th>S no</th>
                <th>USername</th>
                <th>Email</th>
                <th>Password</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="myData"></tbody>
    </table>

    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>Edit Details</h1>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="index">
                    <input type="text" id="editName">
                    <input type="text" id="editEmail">
                    <button class="btn btn-success" onclick="updateData()">Update</button>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <script>

        $('#sign_up').click(function(){
            let un = $('#username').val()
            let em = $('#email').val()
            let ps = $('#password').val()
            // conditon

            $.ajax({
                url:'queries_.php',
                type:'post',
                data:{'register':'signUp','username':un,'email':em,'password':ps},
                success:function(data){
                   alert(data)
                   getData()
                }
            })
        })

        function getData(){
            $.ajax({
                url:'queries_.php',
                type:'get',
                data:{'getData':'get_data'},
                success:function(data){
                   let compiledData = JSON.parse(data)
                   let tag_name = ''
                   for(let i=0 ; i<compiledData.length ; i++){
                       item = compiledData[i]
                       tag_name += '<tr>';
                       tag_name += '<td>'+(i+1)+'</td>';
                       tag_name += '<td>'+item.username+'</td>';
                       tag_name += '<td>'+item.email+'</td>';
                       tag_name += '<td>***********</td>';
                       tag_name += '<td><button onclick="editRecord('+item.id+')">Edit</button><button onclick="deleteData('+item.id+')">Delete</button></td>';
                       tag_name += '</tr>';
                   }
                   $('#myData').html(tag_name)
                }
            })
        }
        getData()

        function editRecord(id){
            $.ajax({
                url:'queries_.php',
                type:'get',
                data:{'edit':id},
                success:function(data){
                    let compiledData = JSON.parse(data)
                   $('#index').val(compiledData.id)
                   $('#editName').val(compiledData.username)
                   $('#editEmail').val(compiledData.email)
                   $('#myModal').modal('show');
                }
            })
        }

        function updateData(){
            let id_     = $('#index').val()
            let upName  = $('#editName').val()
            let upEmail = $('#editEmail').val()
            $.ajax({
                url:'queries_.php',
                type:'post',
                data:{'update':id_,'username':upName,'email':upEmail},
                success:function(data){
                   if(data == 'updated'){
                        $('#myModal').modal('hide');
                        getData()
                   }
                }
            })
            
        }

        function deleteData(id_){
            $.ajax({
                url:'queries_.php',
                type:'post',
                data:{'delete':id_},
                success:function(data){
                   if(data == 'deleted'){
                        alert('Deleted successfully')
                        getData()
                   }
                }
            })
        }

    </script>
    
</body>
</html>