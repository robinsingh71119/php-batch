<?php

class Student{
    public $name;
    public $roll_no;

    function setData($name,$roll_no){
        $this->name     = $name;
        $this->roll_no  = $roll_no;
    }
}

// Single
class Account extends Student{
    public $acc_no;
    function setAcc($acc_no){
        $this->acc_no = $acc_no;
    }

    function showData(){
        echo $this->name;
        echo $this->roll_no;
        echo $this->acc_no;
    }
}

// Multilevel

class OtherDetails extends Account{
    public $address;

    function setAddress($add){
        $this->address = $add;
    }

    function showData(){
        echo $this->name;
        echo $this->roll_no;
        echo $this->acc_no;
        echo $this->address;
    }
}

$o = new OtherDetails();
$o->setData('James','12');
$o->setAcc('15425484512');
$o->setAddress('mohali');
$o->showData();



?>